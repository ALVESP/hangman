﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnClick : MonoBehaviour {

	public Text texto;
	public Button btn;
	public  GameObject userInterface;
	public GameObject vrPlayer;
	public GameObject cm;
	public GameObject Caixa1, Caixa2, Caixa3, Caixa4, Caixa5, Caixa6, Caixa7, Caixa8, Caixa9, Caixa10, Caixa11, Caixa12, Caixa13, Caixa14, Caixa15, Caixa16, Caixa17, Caixa18, Caixa19, Caixa20;
	private int tamanho;
	public char[] word;
	public GameObject textoerro, textoerroo;
	private string letra;

	void TaskOnClick()
	{
		//Debug.Log("Teste: " + letra);
		//Debug.Log("cliquei");
		String palavra = texto.text.ToString(); 
		//Debug.Log("palavra: "+palavra);
		tamanho = palavra.Replace(" ", "").Length;
		//Debug.Log("tamanho: "+tamanho);
		cm.SetActive(false);
		vrPlayer.SetActive(true);
		string[] words = palavra.Split(' ');
        //Debug.Log(words.Length);

        char[] charArr = palavra.ToCharArray();
        for (int i = 0; charArr[i] != ' '; i++)
        {
            words = palavra.Split(' ');
        }


        if (words.Length > 2)
		{
			textoerro.SetActive(true);
		}
		else if(words[0].Length>10||words[1].Length>10)
		{
			textoerroo.SetActive(true);
		}
		else
		{
			userInterface.SetActive(false);
		}
		foreach (string parte in words)
		{

			Debug.Log(parte);
		}
		if (words[0].Length == 1) {
			Caixa1.SetActive(true);
		}
		if (words[0].Length == 2)
		{
			Caixa1.SetActive(true);
			Caixa2.SetActive(true);
		}
		if (words[0].Length == 3)
		{
			Caixa1.SetActive(true);
			Caixa2.SetActive(true);
			Caixa3.SetActive(true);
		}
		if(words[0].Length == 4){
			Caixa1.SetActive(true);
			Caixa2.SetActive(true);
			Caixa3.SetActive(true);
			Caixa4.SetActive(true);
		}
		if(words[0].Length == 5){
			Caixa1.SetActive(true);
			Caixa2.SetActive(true);
			Caixa3.SetActive(true);
			Caixa4.SetActive(true);
			Caixa5.SetActive(true);
		}
		if (words[0].Length == 6){
			Caixa1.SetActive(true);
			Caixa2.SetActive(true);
			Caixa3.SetActive(true);
			Caixa4.SetActive(true);
			Caixa5.SetActive(true);
			Caixa6.SetActive(true);
		}
		if (words[0].Length == 7){
			Caixa1.SetActive(true);
			Caixa2.SetActive(true);
			Caixa3.SetActive(true);
			Caixa4.SetActive(true);
			Caixa5.SetActive(true);
			Caixa6.SetActive(true);
			Caixa7.SetActive(true);
		}
		if (words[0].Length == 8)
		{
			Caixa1.SetActive(true);
			Caixa2.SetActive(true);
			Caixa3.SetActive(true);
			Caixa4.SetActive(true);
			Caixa5.SetActive(true);
			Caixa6.SetActive(true);
			Caixa7.SetActive(true);
			Caixa8.SetActive(true);
		}
		if (words[0].Length == 9)
		{
			Caixa1.SetActive(true);
			Caixa2.SetActive(true);
			Caixa3.SetActive(true);
			Caixa4.SetActive(true);
			Caixa5.SetActive(true);
			Caixa6.SetActive(true);
			Caixa7.SetActive(true);
			Caixa8.SetActive(true);
			Caixa9.SetActive(true);
		}
		if (words[0].Length == 10)
		{
			Caixa1.SetActive(true);
			Caixa2.SetActive(true);
			Caixa3.SetActive(true);
			Caixa4.SetActive(true);
			Caixa5.SetActive(true);
			Caixa6.SetActive(true);
			Caixa7.SetActive(true);
			Caixa8.SetActive(true);
			Caixa9.SetActive(true);
			Caixa10.SetActive(true);

		}
		if (words[1].Length == 1)
		{

			Caixa11.SetActive(true);

		}
		if (words[1].Length == 2)
		{

			Caixa11.SetActive(true);
			Caixa12.SetActive(true);

		}
		if (words[1].Length == 3)
		{
			Caixa11.SetActive(true);
			Caixa12.SetActive(true);
			Caixa13.SetActive(true);

		}
		if (words[1].Length == 4)
		{
			Caixa11.SetActive(true);
			Caixa12.SetActive(true);
			Caixa13.SetActive(true);
			Caixa14.SetActive(true);

		}
		if (words[1].Length == 5)
		{

			Caixa11.SetActive(true);
			Caixa12.SetActive(true);
			Caixa13.SetActive(true);
			Caixa14.SetActive(true);
			Caixa15.SetActive(true);
		}
		if (words[1].Length == 6)
		{
			Caixa11.SetActive(true);
			Caixa12.SetActive(true);
			Caixa13.SetActive(true);
			Caixa14.SetActive(true);
			Caixa15.SetActive(true);
			Caixa16.SetActive(true);

		}
		if (words[1].Length == 7)
		{
			Caixa11.SetActive(true);
			Caixa12.SetActive(true);
			Caixa13.SetActive(true);
			Caixa14.SetActive(true);
			Caixa15.SetActive(true);
			Caixa16.SetActive(true);
			Caixa17.SetActive(true);

		}
		if (words[1].Length == 8){

			Caixa11.SetActive(true);
			Caixa12.SetActive(true);
			Caixa13.SetActive(true);
			Caixa14.SetActive(true);
			Caixa15.SetActive(true);
			Caixa16.SetActive(true);
			Caixa17.SetActive(true);
			Caixa18.SetActive(true);
		}
		if (words[1].Length == 9){

			Caixa11.SetActive(true);
			Caixa12.SetActive(true);
			Caixa13.SetActive(true);
			Caixa14.SetActive(true);
			Caixa15.SetActive(true);
			Caixa16.SetActive(true);
			Caixa17.SetActive(true);
			Caixa18.SetActive(true);
			Caixa19.SetActive(true);
		}
		if (words[1].Length ==10){

			Caixa11.SetActive(true);
			Caixa12.SetActive(true);
			Caixa13.SetActive(true);
			Caixa14.SetActive(true);
			Caixa15.SetActive(true);
			Caixa16.SetActive(true);
			Caixa17.SetActive(true);
			Caixa18.SetActive(true);
			Caixa19.SetActive(true);
			Caixa20.SetActive(true);
		}

	}

	void letralida(string letra){

		string letra_escolhida;
		if (letra == "q" || letra == "w" || letra == "e" || letra == "r" || letra == "t" || letra == "y" || letra == "u" || letra == "i" || letra == "o" || letra == "p" || letra == "a" || letra == "s" || letra == "d" || letra == "f" || letra == "g" || letra == "h" || letra == "j" || letra == "k" || letra == "l" || letra == "ç" || letra == "z" || letra == "x" || letra == "c" || letra == "v" || letra == "b" || letra == "n" || letra == "m" || letra==" ") { // inserir a condição do espaço
			if (letra == " ") {
				Debug.Log ("Não é uma letra!");
			} else {
				//envia a letra
				//nserir.letra = letra;
				Debug.Log ("letra enviada " + letra);
			}	
		} else {
			Debug.Log ("Não é uma letra!");
		}

	}

	// Use this for initialization
	void Start () {

	}

    // Update is called once per frame
    void Update()
    {
        Button botao = btn.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);

		if (Input.GetKeyDown(KeyCode.Joystick1Button0) || Input.GetKey(KeyCode.K))
		{
	        RaycastHit hitInfo = new RaycastHit();
	        bool hit = Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hitInfo);

	        if (hit)
	        {
				Debug.Log("letra: "+hitInfo.transform.gameObject.name);
				letralida(hitInfo.transform.gameObject.name);

	        }

	   }
		/*RaycastHit hitInfo = new RaycastHit();
		bool hit = Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hitInfo); 
		if (hit){

			if (Input.GetKeyDown(KeyCode.Joystick1Button0)|| Input.GetKey(KeyCode.K)){
				letralida(hitInfo.transform.gameObject.name);
			}
		}*/
    }
}
